
O presente capítulo apresenta itens que compõem a metodologia do sistema proposto. Conceitos, métodos, arquitetura, topologia e fluxogramas serão descritos para a especificação do sistema.
Conforme mencionado no capítulo de Introdução, este projeto trata
majoritariamente da descrição de lógica programável (HDL). Sendo assim, mesmo
que o \textit{hardware} não seja o enfoque do projeto, este precisará ser
especificado para o sucesso. Detalhes da metodologia empregada estão na seção de
Método de Desenvolvimento.

\section{MÉTODO DE DESENVOLVIMENTO}

Nesta seção será apresentada a metodologia empregada em vistas do desenvolvimento do projeto proposto, que também será referido por projeto final.
Segundo \cite{LivroEmprestado}, uma boa metodologia de codificação VHDL deve: respeitar as regras da linguagem, especificadas pelos padrões IEEE; gerar códigos de uma aparência comum e similar a outros códigos, com o objetivo de aumentar a familiaridade entre módulos diferentes; ser facilmente legível e mantido, a fim de aumentar a compreensão não somente do autor, mas como qualquer pessoa que leia; ter resultados dentro do esperado, tanto para a geração de códigos de descrição comportamental como para códigos sintetizáveis; evitar estruturas obsoletas, utilizando preferencialmente estruturas contemporâneas e recomendáveis; e coeso, significando que funções, procedimentos e declarações de tipos recorrentes devem ser definidos em pacotes. Adicionalmente, este livro complementa que códigos: simuláveis devem ser eficientes em simulação; e sintetizáveis, obedecer as regras de síntese do fabricante. Entende-se que nesta metodologia de codificação proposta pelo livro, fundamentalmente residem as premissas de portabilidade, modularidade e flexibilidade. Estas premissas significam que o código deve: funcionar para qualquer tecnologia; operar com qualquer fabricante; e ser construído por partes independentes, com interfaces padrões e consequentemente substituíveis. Reconhecendo estas premissas como de bom valor, a linguagem de descrição de \textit{hardware} VHDL será empregada para este projeto proposto.

Para qualquer desenvolvimento de projeto de lógica programável, é necessário inicialmente ser feita a escolha do fabricante de FPGA. Pois cada fabricante de FPGA possui sua própria ferramenta de desenvolvimento. Para o projeto proposto foi escolhido o fabricante Xilinx\textregistered, pois este é o atual líder do mercado de FPGAs e possui ferramentas muito bem conceituadas e tradicionais, tais como Xilinx Vivado\textregistered, simplesmente referida aqui como Vivado. Esta ferramenta faz parte de um pacote nomeado \textit{Vivado HLx Editions}\textregistered, que contém também um ambiente de Síntese de Abstração de Alto Nível (\textit{High-Level Synthesis}, HLS), que permite a geração de códigos de HDL em linguagens de alto nível. HLS é voltada para o projetista com menos experiência em elaborar códigos HDL, porém gera códigos menos otimizados \cite{HLSefficiency} e por este motivo não será utilizada neste trabalho.
O Vivado também inclui Propriedades Intelectuais padrões - em inglês
\textit{Intellectual Proprieties} (IP). O diagrama de blocos, sendo a interface
gráfica do Vivado, não é necessária para o uso destes IPs, estas são também
instanciáveis em código. No contexto de IPs, \textit{soft} significa um
\textit{hardware} implementado utilizando a lógica programável do FPGA; e
\textit{hard} um bloco de primitiva implementado no projeto do \textit{hardware}
FPGA. Alguns blocos \textit{hard} são exigências para certos IPs, como o IP de
PCIe que acompanha o FPGA da Xilinx\textregistered\ - isto não significa a
inexistência de soluções \textit{soft} de PCIe, mas simplesmente não são
acompanhadas com o FPGA em questão. Certas empresas focam parcialmente ou
exclusivamente no fornecimento de IPs criptografados \cite{IntelliProp}.
Soluções de IP \textit{soft} de terceiros são caras e voltadas para o mercado de
grande porte - consequentemente não serão utilizadas neste projeto.
Faz-se importante frisar que, para descrições de \textit{hardware} ligeiramente
complexas, a síntese LTR, a implementação LTR e geração de \textit{bitstreams}
são intensas e demoradas, podendo levar em alguns casos até mais de um mês. Por
isto, é importante que os módulos sejam testados individualmente, para ter-se
uma ideia dos seus funcionamentos.

Partes do projeto que funcionam podem ser integradas ao projeto proposto, sendo
assim mais certeiro quanto a funcionalidade deste. Por este motivo, a
metodologia se baseia em: determinação de requisitos; projetos dos módulos, para
testar pequenas partes do sistema para poder integrar gradativamente; simulações
e testes reais destes módulos; integração dos módulos, a fim de formar um
projeto macro, que será o projeto proposto; e aplicação das métricas, medindo o
sucesso do projeto. A \fig{etapas}
apresenta a metodologia de projeto elaborada que será empregada para o
desenvolvimento do projeto.

\begin{figure}[H]
	\caption{Metodologia empregada no projeto}
	\label{fig:etapas}
	\centering
	\begin{minipage}{.7\textwidth}
		\includegraphics[width=\textwidth]{etapas}
		\fonte{Autor.}
	\end{minipage}
\end{figure}

Na \fig{etapas} é possível compreender melhor o fluxo dos trabalhos.
O projeto inicia-se pela definição dos requisitos e definições de projeto. Após
todas as definições serem feitas, os módulos são elaborados e testados
individualmente. Os testes são feitos em simulação, antes de serem prototipados
na placa de avaliação. Caso um destes testes falharem, voltar-se-á ao projeto do
módulo em questão. Assim, se constrói gradativamente o projeto proposto, por
módulos, até que este possa ser integrado e testado na mesma placa de avaliação.
Caso o projeto proposto não funcionar é estimado o motivo, seja por má
integração dos módulos ou por módulos ``defeituosos'', significando aqueles
módulos que não cumprem a função a qual foram projetados. Caso os módulos
estejam mal integrados nas instanciações, a solução é corrigir o problema no
projeto macro; caso seja o módulo esteja ``defeituoso'', deverá ser retornado ao
projeto do módulo, passando por todo o fluxo novamente. Com o projeto proposto
funcionando, medir-se-á o sucesso conforme os requisitos e as métricas.
Todas as simulações, sejam de estímulos simples, de \textit{test bench}, de
temporização ou de qualquer outra natureza, serão feitas no próprio ambiente
fornecido junto com o Vivado. Para módulos pequenos são feitas simulações
simples de estímulos manuais de entrada, sendo para módulos grandes são feitas
simulações complexas de \textit{test bench}, que geram estímulos de um grande
número de combinações de entrada e em alguns casos a verificação automática de
resultados.
Ao longo de todo o processo será gerado documentação com: textos sobre as
definições de requisitos e projetos; textos sobre os módulos, suas
especificações, funcionalidades, interfaces e como operá-los, similarmente como
é feito para primitivas; e textos sobre o projeto, sobre o funcionamento,
operação e como implementá-lo na placa de avaliação. Toda esta documentação
busca relatar o andamento, as dificuldades encontradas e as conclusões técnicas
para projetos futuros e manutenção deste. Exemplo de preenchimento de
especificação para um módulo se encontra nos apêndices. A metodologia para a
determinação das especificações para estes módulos será indutiva
(\textit{top-down}).
Dando-se por concluída esta seção de Metodologia de Desenvolvimento,
definir-se-á os requisitos na próxima seção.

\section{REQUISITOS}

A razão de realizar este projeto de pesquisa, é fundamentalmente a aplicação.
Porém, afirma-se que a fim de garantir melhoras sensíveis nesta, focar-se-á na
lógica programável e métodos de habilitação tecnológica por \textit{hardware},
para a possibilidade de melhoria do acesso à memória e novas linhas de pesquisa
para o estado da arte.
A metodologia será aplicada para uma solução específica, servindo simultaneamente para a validação do conceito, feito a partir de métricas.
A partir do contexto de pesquisa na área do projeto, feito no capítulo de Estado da Arte, se posiciona o projeto proposto. As premissas de portabilidade, modularidade e flexibilidade serão importadas como requisitos deste projeto.
A flexibilidade permite a expansão da arquitetura proposta, tendo em vista projetos futuros. O requisito de portabilidade serve como justificativa para o não-uso do diagrama de blocos do Vivado. Mesmo que este gere código VHDL, a parte gráfica é compatível unicamente com o fabricante Xilinx\textregistered, não sendo compatível com nenhuma outra ferramenta de qualquer outro fabricante. Já o requisito de modularidade serve para justificar a construção do projeto proposto por módulos. Trocando o FPGA ou a ferramenta, o módulo que deixe de funcionar pode ser substituído por outro módulo de interface e funcionamento idênticos, sendo ajustado a parte incompatível - exemplo de tais módulos são instanciações de primitivas, que variam conforme o FPGA.

O uso de placas de desenvolvimento dá a liberdade da não-preocupação com aspectos que não pertencem ao escopo do projeto, além da disponibilidade de suporte do fabricante. No entanto, caso as escolhas destas placas prontas sejam mal feitas, o projeto completo poderá estar comprometido - como exemplo de uma destas escolhas críticas, uma placa de avaliação cujo FPGA não tenha lógica o suficiente para suportar o projeto.
Por este motivo, é descartado o uso da placa OpenSSD Cosmos+, cujo artigo foi
apresentado no capítulo de Estado da Arte. A Cosmos+ não possui lógica suficiente
para a implementação do projeto proposto, nem outros requisitos essenciais que
estão definidos nesta seção.
No projeto proposto serão utilizadas duas placas: uma placa FPGA de avaliação e outra de compatibilização para SSD PCIe NVMe. Para esta última, a FPGA Drive cumpre o ofício, sendo uma placa de compatibilização do conector FMC e conector M.2 PCIe, a fim de permitir a ligação de dois SSDs com uma placa de avaliação. Tal placa é perfeita para o projeto proposto e será utilizada. Nem todas as placas de avaliações possuem compatibilidade com a FPGA Drive, algumas podendo acessar somente um SSD e outras sendo completamente incompatíveis. A \fig{FPGA_Drive} apresenta esta placa de compatibilização.

\begin{figure}[H]
	\caption{Placa auxiliar de desenvolvimento FPGA Drive}
	\label{fig:FPGA_Drive}
	\centering
	\begin{minipage}{.7\textwidth}
		\includegraphics[width=\textwidth]{FPGA_Drive}
		\fonte{Adaptado de \citeonline{fpga-drive}.}
	\end{minipage}
\end{figure}

Para a realização do projeto proposto será utilizada uma placa de avaliação pronta. Para isto deve ser previsto uma quantidade mínima de lógica disponível para implementar o projeto proposto. A estimativa de ocupação da lógica no FPGA é feita preliminarmente baseado em módulos que deverão ser empregados e informações recolhidas na Internet sobre a ocupação da lógica destes módulos, ou de módulos similares, em FPGAs similares. A placa de avaliação deve possuir \textit{hardware} suficiente para suportar o projeto proposto e os projetos posteriores.
O FPGA deve ter menos de \SI{60}{\percent} de ocupação, pois acima disto, a
ferramenta de compilação demora demasiadamente para gerar o \textit{bitstream}
para prototipação. Caso fosse considerada uma ocupação de \SI{100}{\percent},
estima-se que a prototipação completa levaria até mais de um mês para ser compilada.
Estas estimativas estão presentes nos \ape{EstProp} e \ape{EstKratus} ---
considerando uma ocupação de \SI{60}{\percent} e margem de erro de \SI{20}{\percent}.
A especificação mínima para cumprir os requisitos está listada no \ape{RequisitosSistema}. A placa de avaliação que melhor encaixou-se com esta especificação mínima foi a KCU105 \cite{kcu105}. A KCU105 contém: um FPGA da família Kintex e tecnologia UltraScale, o XCKU040; dois conectores FMC, um de baixa densidade (LPC) e outro de alta densidade (HPC); quatro CIs de memória DRAM DDR, totalizando aproximadamente \SI{2}{\giga\byte}; e conector PCIe \textit{edge connector}, compatível com a geração 3 de oito vias. Faz-se referência a \sse{PCIe} \textit{Peripheral Component Interconnect Express}, que explicita a compatibilidade reversa (\textit{backward compatibility}), significando que a geração 3 da KCU105 poderá funcionar somente com versões acima ou idêntica desta geração de PCIe. A especificação completa da lógica do FPGA contido na KCU105 (XCKU040-2FFVA1156E) está disponível em \cite{UltraScaleSelection} e no \ape{KU040recursos}.
A placa FPGA Drive é compatível com a KCU105, estando disponível ambos os SSDs para o acesso. Na \fig{KCU105} é apresentada esta placa de avaliação.

\begin{figure}[H]
	\caption{Placa de avaliação FPGA KCU105}
	\label{fig:KCU105}
	\centering
	\begin{minipage}{1\textwidth}
		\includegraphics[width=\textwidth]{KCU105.jpeg}
		\fonte{Adaptado de \citeonline{kcu105-samtec}.}
	\end{minipage}
\end{figure}

Neste projeto de pesquisa, o Ext4 será o único FS de compatibilidade obrigatória. O Ext4 é moderno, com uma grande comunidade para o suporte, de uso mais difuso e principalmente relevante para um projeto de pesquisa: com o código aberto. Também neste contexto, será levada em consideração a especificação base 1.3d de NVM Express\texttrademark\ \cite{NVMeSpec} para a realização.
No \qua{RequisitosDeProjeto} consta um agrupamento dos requisitos aqui especificados, seguidos de suas breves justificativas.

\begin{quadro}[H]
 \caption{Requisitos do projeto}
 \label{qua:RequisitosDeProjeto}
\centering
\ABNTEXfontereduzida
\begin{tabular}{|l|l|}
\hline
\multicolumn{1}{|c|}{\textbf{Requisito}} & \multicolumn{1}{c|}{\textbf{Justificativa}} \\ \hline
\begin{tabular}[c]{@{}l@{}}Uso de placas prontas com\\ recursos de \textit{hardware} suficientes\end{tabular} & \begin{tabular}[c]{@{}l@{}}Despreocupação consciente\\ relativo ao \textit{hardware}\end{tabular} \\ \hline
Uso de FPGA com recursos lógicos suficientes & \begin{tabular}[c]{@{}l@{}}Não permitirá a implementação caso não\\ houver recursos suficientes\end{tabular} \\ \hline
Codificação por VHDL & \begin{tabular}[c]{@{}l@{}}Emprego das premissas de flexibilidade,\\ portabilidade e modularidade em codificação\end{tabular} \\ \hline
Flexibilidade & Possibilidade de expansão para projetos futuros \\ \hline
Portabilidade & Compatibilidade com diferentes fabricantes \\ \hline
Modularidade & \begin{tabular}[c]{@{}l@{}}Facilidade de implementação e compreensão\\ simplificadas\end{tabular} \\ \hline
Compatibilidade com Ext4 & \begin{tabular}[c]{@{}l@{}}Necessário o conhecimento do\\ esquema de mapeamento do FS\end{tabular} \\ \hline
Compatibilidade com NVM Express\texttrademark\ 1.3d & \begin{tabular}[c]{@{}l@{}}Necessária a escolha da versão da\\ especificação, sendo esta a mais recente\end{tabular} \\ \hline
\end{tabular}
\fonte{Autor.}
\end{quadro}


Concluída esta definição de requisitos, o projeto proposto será explanado na próxima seção de Sistema Proposto. Nesta seção será definido em detalhes o conteúdo e teor previstos deste.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{SISTEMA PROPOSTO}

No capítulo de Introdução, foi especificado o objetivo deste projeto de
pesquisa, que consiste em propor um conceito de habilitação de acesso à memória.
Para isto, uma \textit{bridge} será implementada como prova do conceito de
habilitação de acesso. Durante a pesquisa relacionada ao estado da arte, não foi
encontrado um artigo que propôs uma solução para gerar estatísticas sobre campos
NVMe, nem tecnologia de \textit{switch} NVMe\footnote{
\ape{artigo1} trata da publicação feita, especificamente do \textit{switch}
NVMe, tópico do capítulo de Análise de Resultados e deste sistema proposta.}.
Conforme demonstrado no capítulo de Estado da Arte, diversas aplicações são
aceleradas com implementações em \textit{hardware}, dado que este
fornece uma velocidade de banda próxima à capacidade teórica
(\textit{wirespeed}). Logo, percebeu-se a possibilidade de interceptação dos
comandos em \textit{hardware} para a interpretação do conteúdo deste. O comando
interpretado poderá então ser disponibilizado para a otimização de acesso e
estatística, com comunicação externa. Na \fig{arquitetura_proposta} abaixo, é
apresentado o planejamento da arquitetura desta \textit{bridge}.

\begin{figure}[H]
	\caption{Arquitetura para a \textit{bridge}}
	\label{fig:arquitetura_proposta}
	\centering
	\begin{minipage}{1\textwidth}
		\includegraphics[width=\textwidth]{arquitetura_proposta}
		\fonte{Autor.}
	\end{minipage}
\end{figure}

Primeiramente, esclarece-se o fluxo dos comandos NVMe dentro da lógica
programável, em seguida, faz-se apontamentos sobre os módulos em questão.
O comando de dado (requisição do \textit{host}) entra no FPGA através do módulo da PCIe \textit{target}, expondo
a instrução NVMe em um formato intermediário, ao próximo módulo: o NVMe
\textit{target}. Este converte deste formato para pacotes NVMe, a ser definido
no capítulo de Análise de Resultados, com o objetivo de simplificar a
complexidade do protocolo NVMe. Após serem processados pelo \textit{switch}
inteligente, estes pacotes são enviados ao módulo de NVMe\footnote{Módulos NVMe
podem ser implementados através de IP comerciais \citeonline{NVMeBridge}} \textit{host}, que
converte novamente ao formato intermediário, readaptando à especificação NVMe.
A partir deste formato, o módulo de PCIe
\textit{host} adequa ao protocolo PCIe, fornecendo os dados à SSD.
Entende-se então, que o \textit{host} comunica-se com o \textit{target}; e
vice-versa. Logo, por \textit{target} compreende-se qualquer módulo que emule
parcelas do SSD, ou dispositivo NVMe. Outrossim, os módulos de PCIe poderão ser formados por
primitivas configuradas especificamente para estas atribuições.
Uma vez entendido o fluxo de comandos NVMe dentro da lógica passa-se à dissertar
sobre a funcionalidade dos módulos.

O módulo de \textit{switch} inteligente é o principal em toda a arquitetura,
pois este cumpre com as tarefas de: captura de campo, contador de comandos; e
arbitração. Captura de campos e contador de comandos NVMe servem para gerar
estatísticas tanto do protocolo NVMe, como do padrão de uso do FS. A arbitração de comandos é
função primária que define qualquer \textit{switch}, e define-se pela
discriminação de comandos, neste caso: os comandos que são enviados ao
\textit{host} e os comandos que são enviados ao \textit{target} alternativo.
Todas estas funcionalidades são configuráveis através da
interface externa em tempo real.
O pacote NVMe discriminado em seus diferentes campos pode ter seu conteúdo
interpretado, baseando-se no leiaute do FS Ext4 - em acordo com a premissa deste
projeto. Sendo assim, é identificado o tipo de bloco de dados sendo lido ou
escrito pelo \textit{host}. Pode-se, então, elaborar diversos processamentos diferentes
para estes comandos discriminados, contudo, propõe-se aqui somente um de
exemplo a seguir.
A captura de campo, juntamente com o contador de comandos, gera estatísticas
de comandos, podendo recriar um histograma dos dados frequentemente acessados,
assim habilitando a área de pesquisa.
Esta mesma estatística pode ser utilizada para o processamento de comandos
discriminados: um armazenamento de blocos da memória frequentemente acessados -
em um \textit{cache} de blocos na memória DRAM.
Os blocos armazenados podem ser reutilizados, uma vez que o \textit{host} faça
uma nova requisição destes, sendo respondida imediatamente pelo próprio
\textit{switch} inteligente. Algumas das consequências imediatas são: a
extensão da vida útil da SSD - por estar sendo menos requisitada; a aceleração
da SSD, pois esta ficaria mais tempo ociosa, deixando o sistema de GC ser
executado neste ócio; e a aceleração na resposta, por não precisar retornar da
SSD, sendo mais rápida pelo tipo da memória utilizada.
O módulo de armazenamento de campo armazena os campos capturados pelo
\textit{switch} inteligente, fornecendo assim os mesmos à interface externa.
Um módulo de controle e estado do sistema é previsto para configuração e
gerenciamento do sistema. Este bloco de controle comunica-se unicamente com a
interface externa, que possui comunicações tanto com os GPIOs da placa KCU105,
tanto com o microcontrolador externo.
O módulo de interface externa implementa um protocolo de comunicação projetado
especificamente para a tarefa de configuração e recuperação de dados.
Esta interface é responsável pela a comunicação entre FPGA e microcontrolador,
sendo que a programação do microcontrolador está fora do escopo deste trabalho.
Aproveitando a RAM existente,
o módulo de espaço de memória contém o controlador de DRAM, podendo assim
fornecer memória de acesso rápido aos módulos de \textit{switch} inteligente e
NVMe \textit{host}. Prevê-se unicamente a necessidade do mesmo se comunicar com
o NVMe \textit{host}, pois este irá emular um \textit{host} - sendo que todas as
filas do NVMe são mantidas no \textit{host}.
Outrossim, um módulo de gerenciamento e distribuição de \textit{clock} é
previsto, contendo uma primitiva de divisão de \textit{clock} e de
\textit{buffers} de distribuição global. Esta arquitetura pode ser implementada
dentro do FPGA, na placa KCU105. A arquitetura do \textit{hardware} a ser
utilizado para a implementação da \textit{bridge} é apresentado na
\fig{hardware_proposto}.

\begin{figure}[H]
	\caption{Arquitetura do \textit{hardware} da \textit{bridge}}
	\label{fig:hardware_proposto}
	\centering
	\begin{minipage}{1\textwidth}
		\includegraphics[width=\textwidth]{hardware_proposto}
		\fonte{Autor.}
	\end{minipage}

	* monoestáveis
\end{figure}

No teste da \textit{bridge}, um computador de testes incorpora a função de
\textit{host}, feita através do \textit{edge connector} da PCIe. Outro
computador sere como interface de configuração do sistema proposto, através
da UART e JTAG. Os recursos do usuário disponíveis da KCU105 estão listados no
\ape{HW_KCU105}. Estes recursos são aproveitados para comunicação com o
sistema e configuração do sistema. Do outro lado existe a placa FPGA Drive, que adequa
a SSD através do conector FMC HPC disponível na KCU105. A FPGA Drive é
transparente para este projeto, servindo meramente como um adaptador. Os
invólucros de memória DRAM de tecnologia DDR4 soldados na placa KCU105 servem
como memória volátil, totalizando \SI{2}{\giga \byte} de armazenamento. Um
cristal de geração de \textit{clock} fixo em \SI{300}{\mega\hertz} alimenta o
bloco de gerenciamento de distribuição de \textit{clock}. O conector FMC LPC
é utilizado para comunicação com o microcontrolador externo. A placa
microprocessadora utilizada será a STM32H743ZIT6U\footnote{Fabricado pela
STMicroelectronics\texttrademark, com microcontrolador ARM\textregistered\
Cortex\textregistered.}\footnote{Manual do usuário disponível em
\cite{Microcontrolador}.}, incorporando a função de microcontrolador. Terminada
esta seção de Sistema Proposto, passar-se-á ao Método de Avaliação, que proporá
critérios para a medição do sucesso do projeto.

\section{MÉTODO DE AVALIAÇÃO}

Esta seção apresentará o método de geração de banco de dados para testes e os critérios de avaliação do projeto.
Tais critérios irão variar conforme o que está sendo avaliado. Para os módulos,
cada simulação será bem-sucedida se este cumprir a função ao qual foi projetado.
Isto inclui, por exemplo para um módulo de gerenciador de \textit{clock} com
restrições de \textit{jitter}, uma simulação adicional de cumprimento destas
restrições na distribuição interna no FPGA. Estas restrições deverão constar na
especificação do módulo.
Na prototipação destes, deverá ser feita uma adaptação para poder processar na
placa KCU105. Esta adaptação não é objeto de avaliação, consequentemente não
será avaliada. Caso o módulo seja simples, esta etapa não será necessária. O
grau de simplicidade do módulo deverá ser declarado em sua especificação.
Nesta prototipação poderão ser testados múltiplos módulos em conjunto. Alguns
módulos não podem ser testados separadamente, tal como o módulo de comunicação
por PCIe, necessitando o módulo de instanciação do \textit{buffer} específico do
\textit{clock}, evidenciando um sequenciamento de testes.
Uma vez que todos os módulos foram testados e prototipados na placa KCU105, o
projeto poderá ser integrado e prototipado. Identicamente à simulação dos
módulos, o protótipo do projeto proposto passará o teste se este cumprir a
função ao qual foi projetado.
Percebe-se que a função dos módulos e do projeto servem como base dos critérios
de avaliação. Consequentemente, estas funções devem ser bem definidas. As
funções dos módulos são especificadas em suas documentações individualmente,
vide a importância da documentação. Exemplo de especificação de módulo consta no
\ape{EspecCapturaCampo}.

No \ape{banco_de_dados_metodo} é apresentado o método de geração de comandos
pseudo-aleatórios. O intuito deste método é obter um conjunto de comandos de
submissão (direção \textit{host} para SSD) de padrão NVMe, cujos conteúdos são
conhecidos, havendo a possibilidade de gerar comandos específicos - com
conteúdos administráveis. O resultado dos comandos gerados seriam arquivos de
texto em conjunto com sua documentação. Arquivos de texto (``.txt'') podem ser
introduzidos em simulações do tipo \textit{test bench}. O processo começa
definindo-se um número $N$ arbitrário de comandos. Para iniciar-se a contagem, é
introduzida uma variável $n$ que conta o número de iterações realizadas. Um
bloco de função de comando pode ser operado pseudo-aleatoriamente, ou por
instrução direta. Por função de comando entende-se: de leitura, de escrita, ou
de qualquer outra definida na especificação NVMe. Outras opções peculiares de
cada tipo de comando é operada de forma similar, gerando ao mesmo tempo a
documentação. Caso a função do comando necessite conteúdo adicional, por exemplo
de escrita, um conjunto de \textit{bits} pseudo-aleatórios poderão ser
fornecidos. Isto se repete a cada iteração, incrementando o contador a cada vez,
até que se chegue ao valor determinado de $N$.


Dado que o sistema de arquivos que será utilizado é o Ext4, o GNU/Linux deverá
ser o sistema operacional para testes, por ser aberto e difundido.
Adicionalmente, o GNU/Linux é compatível com todas as ferramentas necessárias.
Terminado este capítulo de Metodologia, inicia-se o capítulo de Análise de
Resultados.

