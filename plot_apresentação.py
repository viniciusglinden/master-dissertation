import matplotlib.pyplot as plt
# import numpy as np
from matplotlib.ticker import (MultipleLocator, FormatStrFormatter, AutoMinorLocator)

# Armazenamento [Gi] Ano Custo Armazenamento US$/GB fonte
# SSDs
# 0.0439453 1978 400000 45MiB 9102224.811 http://www.storagesearch.com/storagetek.html
# 0.0283203 1991 1000 29MiB 35310.36041 https://www.computerhistory.org/storageengine/solid-state-drive-module-demonstrated/
# 15728.64 2016 10000 15.36TiB 0.6357828776 https://www.anandtech.com/show/10555/seagate-introduces-10gbs-pcie-ssd-and-60tb-sas-ssd
# 102400 2018 40000 100TiB 0.390625 https://www.techspot.com/amp/news/85911-nimbus-data-100tb-ssd-can-yours-40000.tml
# 2048 2020 249.99 2TiB 0.1220654297 https://www.anandtech.com/show/9799/best-ssds
# HDD
# 1024 2016 50 1TiB 0.048828125 https://hdd.userbenchmark.com/Seagate-Barracuda-1TB-2016/Rating/3896
# 1024 2020 47.99 1TiB 0.04686523438 https://www.amazon.com/Seagate-BarraCuda-Internal-Drive-3-5-Inch/dp/B07D99KFPK/ref=sr_1_3?crid=35TI76BN3SZ0N&dchild=1&keywords=internal+hard+drive+1tb&qid=1596724521&sprefix=Internarl+har%2Caps%2C364&sr=8-3h

plt.rcParams.update({'font.size': 25})

year = [2016, 2018, 2020]
SSD = [0.6357828776, 0.390625, 0.1220654297]
HDD = [0.048828125, 0.0478466797, 0.04686523438]

fig = plt.figure()
cost = fig.add_subplot(111)
cost.plot(year, SSD, label='SSD', linewidth=5, marker='^', markersize=12)
cost.plot(year, HDD, label='HDD', linewidth=5, marker='^', markersize=12)
cost.text(year[-1], SSD[-1], '0,122')
cost.text(year[-1], HDD[-1], '0,047')

cost.xaxis.set_major_formatter(FormatStrFormatter('%d'))
cost.yaxis.set_major_formatter(FormatStrFormatter('%1.1f'))
cost.grid(True)

plt.xlabel("Ano")
plt.ylabel("US$ por GiB")
plt.title("Histórico de preço")
plt.legend()

plt.show()
