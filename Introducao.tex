
Memórias digitais nos últimos anos vêm se desenvolvendo em três aspectos
principais: menor custo; maior velocidade de banda; e maior capacidade de
armazenamento. A principal tecnologia atualmente em uso, a Unidade de Disco Rígido,
ou \textit{Hard Disk Drive} (HDD) em inglês, consiste em um disco magnético
rotacionando em alta velocidade, provendo armazenamento a baixo custo e alta
capacidade. Porém, dentre os efeitos indesejados causados pela parte mecânica,
está a limitação da velocidade de banda. Uma das alternativas à tecnologia
citada é a Unidade de Estado Sólido, \textit{Solid State Drive} (SSD). Pela
natureza da tecnologia, a SSD supera a HDD em velocidade de banda e, com o
avanço das pesquisas na área, tende a ultrapassá-la em custo e capacidade de
armazenamento, viabilizando seu uso comercial disseminado. Para aproveitar ao
máximo a tecnologia SSD, criou-se o Protocolo de Comunicação para Memórias
Não-Voláteis, \textit{Non-Volatile Memory Express} (NVMe), que desbloqueou
parcialmente as limitações de banda previamente impostas em \textit{software}.
Não obstante, a tecnologia \textit{flash}, empregada em SSDs, pode ser ainda
mais aperfeiçoada. Para tanto, faz-se necessário um dispositivo que implemente
uma plataforma para o estudo do tráfego de informações e possível otimização de acesso.

Quando se deseja otimizar o processamento de algum algoritmo específico, uma boa
solução é a implementação deste algoritmo em \textit{hardware}, pois tais
implementações apresentam uma banda superior ou igual às implementações
equivalentes em \textit{software}. O \textit{hardware} é planejado
especificamente para a tarefa, significando que este pode executar múltiplos
passos em um ciclo de \textit{clock}, em um grau de paralelismo adequado --- além
da possibilidade de implementação de lógica assíncrona. Por conseguinte, é dito
que o \textit{hardware} provê a chamada ``velocidade de fio''
(\textit{wirespeed}), significando uma velocidade de banda próxima à capacidade
teórica. Outrossim, para execução em \textit{hardware} é comum utilizar
Dispositivos de Lógica Programável, ou \textit{Field-Programmable Gate Array}
(FPGA) em inglês. FPGAs permitem a implementação reprogramável de um
\textit{hardware} digital --- diferentemente do Circuito Integrado para Aplicação
Específica, \textit{Application Specific Integrated Circuit} (ASIC), incapaz de
reprogramação. Portanto, FPGAs estão sendo cada vez mais utilizados para
acelerações. Um primeiro caso de uso de aceleração de algoritmos específicos por
\textit{hardware} são os \textit{racks} da Microsoft\texttrademark, que empregam
FPGAs para a aceleração da sua ferramenta de busca na Internet \cite{microsoft}.
Outros exemplos serão providos no capítulo de Estado da Arte.

% proposta
O presente projeto de pesquisa propõe uma combinação da tecnologia
\textit{flash} emergente, em formato SSD, com lógica programável. Este projeto
deverá ser implementado futuramente em FPGA. Para isto, será proposta uma
\textit{bridge} de comunicação entre \textit{host} e SSD.
% nomenclatura
Uma \textit{bridge} é qualquer dispositivo eletrônico que governa o fluxo de
dados entre os pontos que se conecta, sendo capaz de observar o tráfego,
identificando informações relevantes. Já um \textit{host} define-se por
qualquer servidor, computador ou qualquer outro dispositivo eletrônico capaz de
se comunicar com a \textit{bridge} através de sua interface respectiva, implementando
as funções necessárias de emissão e recepção de dados, através de seu sistema
operacional.
% especificação
O \textit{host} perceberá a \textit{bridge} conectada como uma SSD.
A implementação desta \textit{bridge} em
\textit{hardware} dar-se-á pelo uso da placa de avaliação FPGA. Uma placa de
avaliação FPGA define-se por ser pré-projetada e fornecida por terceiros,
normalmente sendo os próprios fabricantes do FPGA, com o objetivo de
desenvolvimento de projetos em plataforma FPGA, tendo como principal aspecto o
seu reuso e versatilidade.
% objetivo
Inicialmente, a função principal desta \textit{bridge}, será de permitir a
comunicação direta entre \textit{host} e SSD, sem alterações dos dados, com a leitura de
certos campos relevantes para diferentes características de cargas de trabalhos
(\textit{workload}) para as memórias. Tal \textit{bridge} poderá coletar
informações em tempo real sobre o protocolo NVMe, podendo gerar futuramente
estatísticas relevantes a outros pesquisadores. Uma comunicação externa com um
microcontrolador deverá existir, sendo que este irá capturar as informações
geradas pelo FPGA. Chama-se esta \textit{bridge} de habilitadora, pois ela
habilitará: pesquisas sobre tráfego entre ambos os pontos, podendo gerados
trabalhos científicos sobre os padrões de acesso do SSD, sobre a estrutura dos
dados que transitam, entre outros; e a futura otimização do acesso. Estão
previstas como contribuições científicas deste trabalho:

\begin{itemize}
	\item Planejamento de uma arquitetura flexível de \textit{bridge} para SSDs;
	\item O desenvolvimento desta \textit{bridge};
	\item Habilitar a pesquisa sobre o tráfego de dados entre \textit{host} e SSD;
	\item Possibilitar a otimização de acesso à memória;
	\item Desenvolvimento de um ambiente de simulação de partes internas da \textit{bridge};
	\item Especificação do \textit{hardware} futuramente empregado;
	\item Disponibilização dos códigos em uma plataforma aberta à pesquisadores;
	\item Habilitar projetos futuros, que poderão basear neste.
\end{itemize}

Definições adicionais serão necessárias para o entendimento do texto a vir, são
elas: metadado, aplicativo, aplicação e módulos. Primeiramente, um metadado
significa qualquer descrição sobre características de um dado, por exemplo:
tamanho, tipo de arquivo, data de criação, proprietário, entre outros
\cite{priberamMetadado}; a segunda definição é a de aplicativo, sendo um
programa informático que visa facilitar a realização de uma tarefa em um
dispositivo qualquer \cite{priberamAplicativo}; A terceira definição trata-se do
significado de aplicação, que equivale à camada de abstração --- que permite
transmitir informações de um nível menor adaptando-as para uma abstração de
nível mais alto e vice-versa. Por último, a definição de módulos, que são
``blocos'' de funções únicas que constroem qualquer sistema de Lógica de Tempo
Real (LTR). São exemplos destes módulos: instanciações de primitivas,
gerenciador de relógio (\textit{clock}), \textit{buffers} de dados,
serializadores, entre outras implementações. Módulos podem ser feitos de outros
módulos, não havendo qualquer restrição.

O texto está organizado por capítulos, sendo: Conceitos Básicos, que apresentará
o conceitos para a boa compreensão do trabalho; após, o Estado da Arte, que
dissertará sobre trabalhos relacionados; em seguida, na Metodologia, serão
apresentados, em ordem, o método empregado, os requisitos impostos, o sistema
proposto e os critérios de avaliação; em seguida, na Análise de Resultados,
serão descritos conceitos desenvolvidos para a implementação do sistema
proposto, após, serão apresentadas as três soluções aos objetivos descritos,
comprovando o funcionamento destas; por último, o capítulo de Conclusão tratá
uma reflexão final, analisando a observância aos objetivos conforme as métricas
impostas, terminando por sugestões de trabalhos futuros. Apêndices de auxílio ao
texto serão apresentados. Terminado este capítulo de Introdução, iniciar-se-á
pelo capítulo de Conceitos Básicos, que trará fundamentos para a boa compreensão
do projeto.

